"use strict";
const express = require("express");
const app = express();

app.use(express.static("www"));
app.use((req,res)=>{
	res.status(404).json({data:'File not found :('});
});
const PORT = process.env.PORT || 3000;
app.listen(PORT,()=>{
	console.log("Listening at port",PORT);
});
